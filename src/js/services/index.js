import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);

import UserService from './user.service';
servicesModule.service('User',UserService);

import JWTService from './jwt.service';
servicesModule.service('JWT', JWTService);

import ProfileService from './profile.service';
servicesModule.service('Profile', ProfileService);

import ArticleSerice from './articles.service';
servicesModule.service('Articles', ArticleSerice);

import CommentService from './comments.service';
servicesModule.service('Comments', CommentService);

import TagsService from './tags.service';
servicesModule.service('Tags', TagsService);

export default servicesModule;
