export default class Articles {
    constructor(AppConstants, $http, $q) {
        'ngInject';

        this._AppConstants = AppConstants;
        this._$http = $http;
        this._$q = $q;

    }

    add(slug, payload) {
        return this._$http({
            url: `${this._AppConstants.api}/articles/${slug}/comments`,
            method: 'POST',
            data: {comment: {body: payload}}
        }).then((res) => res.data.comment);
    }

    latest() {
        return this._$http({
            url: `${this._AppConstants.api}/latest`,
            method: 'GET'
        }).then((res) => res.data.comments);
    }


    getAll(slug) {
        return this._$http({
            url: `${this._AppConstants.api}/articles/${slug}/comments`,
            method: 'GET'
        }).then((res) => res.data.comments);
    }

    destroy(slug, comment) {
        return this._$http({
            url: `${this._AppConstants.api}/articles/${slug}/comments/${comment}`,
            method: 'DELETE'
        });
    }
}