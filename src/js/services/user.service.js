export default class User {
    constructor(JWT, AppConstants, $http, $state, $q) {
        'ngInject';
        this._AppConstants = AppConstants;
        this._$http = $http;
        this._JWT = JWT;
        this.current = null;
        this._$state = $state;
        this._$q = $q;
    }

    // Try to authenticate by registering or logging in
    attemptAuth(type, credentials) {
        let route = (type === 'login') ? '/login' : '';
        return this._$http({
            url: this._AppConstants.api + '/users' + route,
            method: 'POST',
            data: {
                user: credentials
            }
        }).then(
            // On success...
            (res) => {
                // Set the JWT token
                this._JWT.save(res.data.user.token);

                // Store the user's info for easy lookup
                this.current = res.data.user;

                return res;
            }
        );
    }

    // Log out
    logout() {
        this.current = null;
        this._JWT.destroy();

        // Do a hard reload to flush out the data
        this._$state.go(this._$state.$current, null, {reload: true});
    }

    verifyAuth() {
        let defered = this._$q.defer();

        // Check for JWT token first
        if (!this._JWT.get()) {
            defered.resolve(false);
            return defered.promise;
        }

        // If there is an user
        if (this.current) {
            defered.resolve(true);
        }

        // If current user isn't set, get it from the server
        // If server doesn't 401, set current user and resolve promise

        else {
            this._$http({
                url: this._AppConstants.api + '/user',
                method: 'GET'
            }).then(
                (res) => {
                    this.current = res.data.user;
                    defered.resolve(true);
                },
                // if error
                () => {
                    this._JWT.destroy();
                    defered.resolve(false);
                }
            )
        }
        return defered.promise;
    }

    ensureAuthIs(bool) {
        let deferred = this._$q.defer();

        this.verifyAuth().then((authValid) => {
            // if it's the opposite, redirect home
            if (authValid !== bool) {
                this._$state.go('app.home');
                deferred.resolve(false);
            } else {
                deferred.resolve(true);
            }
        });

        return deferred.promise;
    }

    // updates the current user
    update(fields) {
        return this._$http({
            url: this._AppConstants.api + '/user',
            method: 'PUT',
            data: {
                user: fields
            }
        }).then(
            (res) => {
                this.current = res.data.user;
                return res.data.user;
            }
        );
    }

}