class SettingsCtrl {
    constructor(User, $state) {
        'ngInject';

        this._User = User;
        this._$state = $state;

        this.errors = {};

        this.formData = {
            email: User.current.email,
            bio: User.current.bio,
            image: User.current.image,
            username: User.current.username
        };
    }

    logout() {
        this._User.logout();
    }

    submitForm(){
        this._User.update(this.formData).then(
            (user) => {
                this._$state.go('app.profile.main', {username:user.username});
            })
            .catch((err) => {
                this.errors = err.data.errors;
            });
    }

}


export default SettingsCtrl;