class HomeCtrl {
    constructor(AppConstants, Tags, User, $scope, Comments, Articles, JWT) {
        'ngInject';

        this.appName = AppConstants.appName;
        this._$scope = $scope;

        Tags.getAll()
            .then(
                (tags) => {
                    this.tagsLoaded = true;
                    this.tags = tags;
                }
            );

        Comments.latest()
            .then(
                (comments) =>{
                    this.commentsLoaded = true;
                    this.comments = comments;
                }
            );

        if(JWT.get()){
            Articles.recommendations()
                .then(
                    (articles) => {
                        this.recommendationsLoaded = true;
                        this.recommendations = articles.data.articles;
                    }
                );
        }

        //Set the current list of tags to either feed or global
        this.listConfig = {
            type: User.current ? 'feed' : 'all'
        };
    }

    changeList(newList){
        this._$scope.$broadcast('setListTo', newList);
    }


}

export default HomeCtrl;
