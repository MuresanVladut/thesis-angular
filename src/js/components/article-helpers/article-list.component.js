class ArticleListCtrl{
    constructor(Articles, $scope, User){
        'ngInject';

        this._Articles = Articles;
        this._User = User;

        $scope.$on('setPageTo', (ev, number) =>{
           this.setPageTo(number);
        });

        $scope.$on('setListTo', (ev, newList) =>{
            this.setListTo(newList);
        });
    }

    setPageTo(number){
        this.listConfig.currentPage = number;
        this.runQuery();
    }

    $onInit(){
        this.setListTo(this.listConfig);
        this.currentUser = this._User.current;
    }

    setListTo(newList){
        //Set the current list to an empty array
        this.list = [];

        //Set the listConfig to the new listConfig
        this.listConfig = newList;
        this.runQuery();
    }

    runQuery(){
        //Show the loading indicator
        this.loading = true;

        //Create an object for this query
        let queryConfig = {
            type: this.listConfig.type,
            filters: this.listConfig.filters || {}
        };

        //Set the query limit filter from the component's attribute

        queryConfig.filters.limit = this.limit;

        //If there is no page set, set it to 1
        if(!this.listConfig.currentPage){
            this.listConfig.currentPage = 1;
        }

        //Add the offset filter
        queryConfig.filters.offset = (this.limit * (this.listConfig.currentPage -1));

        this._Articles
            .query(queryConfig)
            .then(
                (res) => {
                    this.loading = false;

                    //Update the list
                    this.list = res.articles;

                    this.listConfig.totalPages = Math.ceil(res.articlesCount / this.limit);
                }
            );
    }
}

let ArticleList = {
    bindings: {
        limit: '=',
        listConfig: '='
    },
    controller: ArticleListCtrl,
    templateUrl: 'components/article-helpers/article-list.html'
};

export default ArticleList;