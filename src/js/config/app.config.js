import authInterceptor from './auth.interceptor';

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider, $qProvider) {
    'ngInject';

  //push the interceptor for auth
    $httpProvider.interceptors.push(authInterceptor);


  $stateProvider
    .state('app', {
      abstract: true,
      templateUrl: 'layout/app-view.html',
        resolve: {
          auth: function(User){
            return User.verifyAuth();
          }
        }
    });
    $qProvider.errorOnUnhandledRejections(false);

  $urlRouterProvider.otherwise('/');

}

export default AppConfig;
