function ProfileConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.profile', {
            url: '/@:username',
            controller: 'ProfileCtrl',
            abstract: true,
            controllerAs: '$ctrl',
            templateUrl: 'profile/profile.html',
            resolve: {
                profile: function (Profile, $state, $stateParams) {
                    return Profile.get($stateParams.username).then(
                        (profile) => profile,
                        (err) => $state.go('app.home')
                    )
                }
            }
        })
        .state('app.profile.main', {
            url: '',
            controller: 'ProfileArticlesCtrl as $ctrl',
            templateUrl: 'profile/profile-articles.html',
            title: 'Profile'
        })
        .state('app.profile.favorites', {
            url: '/favorites',
            controller: 'ProfileArticlesCtrl as $ctrl',
            templateUrl: 'profile/profile-articles.html',
            title: 'Favorites'
        });

}

export default ProfileConfig;
