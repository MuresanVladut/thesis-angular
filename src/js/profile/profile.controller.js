class ProfileCtrl {
  constructor(profile, User) {
    'ngInject';
    this._profile = profile;
    this._User = User;

    if(User.current){
      this.isUser = (User.current.username === this._profile.username);
    }
    else {
      this.isUser = false;
    }
  }
}


export default ProfileCtrl;
