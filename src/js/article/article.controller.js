import marked from 'marked';
class ArticleCtrl {
    constructor(article, $sce, $rootScope, User, Comments) {
        'ngInject';
        this.article = article;
        // Update the title of this page
        $rootScope.setPageTitle(this.article.title);


        this._Comments = Comments;


        this._Comments.getAll(this.article.slug).then(
            (comment) => this.comments = comment
        );
        this.currentUser = User.current;
        // Transform the markdown into HTML
        this.article.body = $sce.trustAsHtml(marked(this.article.body, {sanitize: true}));
        this.resetCommentForm();
    }

    resetCommentForm(){
        this.commentForm = {
            isSubmitting: false,
            body: '',
            errors: []
        }
    }

    addComment(){
        this.commentForm.isSubmitting = true;
        this._Comments.add(this.article.slug, this.commentForm.body).then(
            (comment) => {
                this.comments.unshift(comment);
                this.resetCommentForm();
            },
            (err) => {
                this.commentForm.isSubmitting = false;
                this.commentForm.errors = err.data.errors;
            }
        )
    }
    deleteComment(comment, index){
        this._Comments.destroy(this.article.slug, comment).then(
            () => {
                this.comments.splice(index, 1);
            }
        )
    }
}


export default ArticleCtrl;
